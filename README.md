# andProject

HTTP Verb | Endpoint | Parameter | Description
---------: | :-----: | :-----: | :-----:
GET|/customers|NONE|Fetches all the customers|
GET|/customers/:id|:id|Fetches a customer with id
GET|/phoneNumbers|NONE|Fetches all phone numbers
GET|/phoneNumbers/:customerId|:customerId|Fetches the phone numbers of a customer
GET|/phoneNumbers/:id/activate|:id|Activates a specific phone number

### How to build?
* npm install

### How to run?
* npm start

### How to test?
* npm test