const express = require('express');

const router = require('./src');

// Basic config DO NOT change
const server = express();
const port = process.env.PORT || 3000

server.use(router);

// Server initialize
server.listen(port, (err) => {
    if (err) {
        return console.log(err)
    }

    return console.log(`server is listening on ${port}`)
})

module.exports = server;