const router = require('express').Router();

const customersRoute = require('./routes/customer.route');
const phoneNumbersRoute = require('./routes/phoneNumber.route');

customersRoute(router);
phoneNumbersRoute(router);

module.exports = router;