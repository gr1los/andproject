const { customers } = require('../../fixtures');

class CustomerControllerClass {
    constructor() { }

    fetchAll(req, res) {
        return res.json(customers);
    }

    fetchById(req, res) {
        return res.json(
            customers.find(el => el.id === Number(req.params.id))
        );
    }
}

module.exports = new CustomerControllerClass();