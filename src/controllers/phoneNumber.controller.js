const { customers, phoneNumbers } = require('../../fixtures');

class PhonenumberControllerClass {
    constructor() { }

    fetchAll(req, res) {
        return res.json(phoneNumbers);
    }

    fetchByCustomerId(req, res) {
        const customer = customers.find(
            el => el.id === Number(req.params.customerId)
        );

        return res.json(
            phoneNumbers.find(el => el.id === customer.phoneNumberId)
        );
    }

    activateNumber(req, res) {
        const phoneNum = phoneNumbers.find(el => el.id === Number(req.params.id));

        phoneNum.activated = true;

        return res.json(phoneNum);
    }
}

module.exports = new PhonenumberControllerClass();