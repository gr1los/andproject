const {
    fetchAll, fetchById
} = require('../controllers/customer.controller');

module.exports = (router) => {
    router.get('/customers', fetchAll);
    router.get('/customers/:id', fetchById);
}