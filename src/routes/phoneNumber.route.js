const {
    fetchAll, fetchByCustomerId, activateNumber
} = require('../controllers/phoneNumber.controller');

module.exports = (router) => {
    router.get('/phoneNumbers', fetchAll)
    router.get('/phoneNumbers/:customerId', fetchByCustomerId);
    router.get('/phoneNumbers/:id/activate', activateNumber);
}