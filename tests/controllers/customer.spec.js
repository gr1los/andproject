const chai = require('chai');
const sinonChai = require('sinon-chai');
const { mockReq, mockRes } = require('sinon-express-mock');
chai.use(sinonChai);

const customerController = require('../../src/controllers/customer.controller');
const { customers } = require('../../fixtures');

const { expect } = chai;

describe('#Customer Controller', () => {
    it('should fetch all the customers', () => {
        const req = mockReq();
        const res = mockRes();

        customerController.fetchAll(req, res);

        expect(res.json).to.be.calledWith(customers);
    });

    it('should fetch the first customer', () => {
        const req = mockReq({ params: { id: 1 }});
        const res = mockRes();

        customerController.fetchById(req, res);

        expect(res.json).to.be.calledOnceWith(customers[0]);
    });
});