const chai = require('chai');
const sinonChai = require('sinon-chai');
const { mockReq, mockRes } = require('sinon-express-mock');
chai.use(sinonChai);

const phoneNumberController = require('../../src/controllers/phoneNumber.controller');
const { phoneNumbers } = require('../../fixtures');

const { expect } = chai;

describe('#PhoneNumber Controller', () => {
    it('should fetch all the numbers', () => {
        const req = mockReq();
        const res = mockRes();

        phoneNumberController.fetchAll(req, res);

        expect(res.json).to.be.calledWith(phoneNumbers);
    });

    it('should fetch number of the first customer', () => {
        const req = mockReq({ params: { customerId: 1 }});
        const res = mockRes();

        phoneNumberController.fetchByCustomerId(req, res);

        expect(res.json).to.be.calledWith(phoneNumbers[0]);
    });

    it('should fetch an activated number', () => {
        const req = mockReq({ params: { id: 1 }});
        const res = mockRes();

        phoneNumberController.activateNumber(req, res);

        expect(res.json).to.be.calledWith(phoneNumbers[0]);
    });
});