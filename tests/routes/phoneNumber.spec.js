const request = require('supertest');
const chai = require('chai');
const sinonChai = require('sinon-chai');
chai.use(sinonChai);

const { expect } = chai;

const { phoneNumbers } = require('../../fixtures');

const server = require('../../index');

describe('GET /phoneNumbers', () => {
    it(`responds with ${phoneNumbers.length} json objects`, () => {
        request(server)
            .get('/phoneNumbers')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, res) => {
                if (err) throw err;
                expect(res.body.length === phoneNumbers.length);
            })
    });

    it('responds with 1 json object', () => {
        request(server)
            .get('/phoneNumbers/1')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, res) => {
                if (err) throw err;
                expect(res.body.length === 1);
            });
    });

    it('responds with 1 activated phone number', () => {
        request(server)
            .get('/phoneNumbers/1/activate')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, res) => {
                if (err) throw err;
                expect(res.body.length === 1);
                expect(res.body.activated);
            });
    });
});