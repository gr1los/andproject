const request = require('supertest');
const chai = require('chai');
const sinonChai = require('sinon-chai');
chai.use(sinonChai);

const { expect } = chai;

const { customers } = require('../../fixtures');

const server = require('../../index');

describe('GET /customers', () => {
    it(`responds with ${customers.length} json objects`, () => {
        request(server)
            .get('/customers')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, res) => {
                if (err) throw err;
                expect(res.body.length === customers.length)
            })
    });

    it('responds with 1 json object', () => {
        request(server)
            .get('/customers/1')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, res) => {
                if (err) throw err;
                expect(res.body.length === 1)
            });
    });
});